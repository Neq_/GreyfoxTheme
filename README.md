A simple grey "Web 1.0-inspired" custom CSS for e621.net
> _"What e621 may have looked like in 1996."_

<img src="https://i.imgur.com/43qriYv.png" width="300"/>


**How to use:**

1. Copy the contents of the [**Greyfox.css**](https://gitlab.com/Neq_/GreyfoxTheme/-/raw/master/Greyfox.css) file to your clipboard
2. Paste it into the text box found on e621 in **Account → Settings → Advanced → Custom CSS style**
3. Click 'Submit'.

---
Made by Caroway: https://e621.net/users/9259

[Send me a Dmail](https://e621.net/dmails/new?dmail[to_id]=9259&dmail[title]=Greyfox%20Theme) for comments, help or suggestions.
